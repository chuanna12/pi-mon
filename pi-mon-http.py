import psutil
import os
import subprocess
from flask import Flask
import datetime
import re
import time

app = Flask(__name__)
#app.debug = True # Uncomment to debug

def sys_cmd(cmd, debug=0):
    if cmd[:5] == "START":
        os.system(cmd)
    else:
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        tmpstr = str(stdout)
        if debug:
            tmpstr = tmpstr.replace("b'","")
            tmpstr = tmpstr.replace("b\"","")
            tmpstr = tmpstr.replace("'","")
            tmpstr = tmpstr.replace("\"","")
            # tmparr = tmpstr.split("\\r\\n")
            tmparr = tmpstr.split("\\n")
            for line in tmparr:
                #line = line[2:-1]
                # line = line.replace("\\r\\n","")
                line = line.replace("\\n","")
                print(line)
            #print(str(stdout))
        return tmparr

@app.route('/')
def home():
    return 'System Stats!'

@app.route('/cpu')
def cpu():
    return str(psutil.cpu_percent()) + '%'

@app.route('/memory')
def memory():
    memory = psutil.virtual_memory()
    # Divide from Bytes -> KB -> MB
    available = round(memory.available/1024.0/1024.0,1)
    total = round(memory.total/1024.0/1024.0,1)
    return str(available) + 'MB free / ' + str(total) + 'MB total ( ' + str(memory.percent) + '% )'

@app.route('/disk')
def disk():
    disk = psutil.disk_usage('/')
    # Divide from Bytes -> KB -> MB -> GB
    free = round(disk.free/1024.0/1024.0/1024.0,1)
    total = round(disk.total/1024.0/1024.0/1024.0,1)
    return str(free) + 'GB free / ' + str(total) + 'GB total ( ' + str(disk.percent) + '% )'

@app.route('/cputemp')
def cputemp():
    ret = sys_cmd("cat /sys/class/thermal/thermal_zone0/temp", 1)
    # print(ret)
    return str(float(ret[0]) / 1000) # 'C

@app.route('/gputemp')
def gputemp():
    ret = sys_cmd("/opt/vc/bin/vcgencmd measure_temp", 1)
    return ret[0][5:-1] # 'C

if __name__ == '__main__':
    app.run(host='0.0.0.0')